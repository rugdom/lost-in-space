package edu.ktu.guessthenumber;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class PlayerResult implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private int age;
    private int winCount;
    private int loseCount;

    public PlayerResult(String name, int age, int winCount, int loseCount) {
        this.name = name;
        this.age = age;
        this.winCount = winCount;
        this.loseCount = loseCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWinCount() {
        return winCount;
    }

    public void setWinCount(int winCount) {
        this.winCount = winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public void setLoseCount(int loseCount) {
        this.loseCount = loseCount;
    }
}
