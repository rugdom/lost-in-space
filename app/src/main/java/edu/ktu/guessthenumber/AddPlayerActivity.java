package edu.ktu.guessthenumber;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class AddPlayerActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "edu.ktu.guessthenumber.EXTRA_NAME";
    public static final String EXTRA_AGE = "edu.ktu.guessthenumber.EXTRA_AGE";
    private EditText nameField;
    private EditText ageField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_player);

        nameField = findViewById(R.id.player_name_field);
        ageField = findViewById(R.id.player_age_field);
    }

    public void savePlayer(View view) {
        if(nameField.getText().toString().trim().isEmpty() || ageField.getText().toString().trim().isEmpty()){
            Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
            return;
        }

        String playerName = nameField.getText().toString();
        int playerAge = Integer.parseInt(ageField.getText().toString());

        Intent data = new Intent();
        data.putExtra(EXTRA_NAME, playerName);
        data.putExtra(EXTRA_AGE, playerAge);
        setResult(RESULT_OK, data);
        finish();
    }
}

