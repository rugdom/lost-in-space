package edu.ktu.guessthenumber;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PlayerResultRepository {

    private PlayerResultDao playerResultDao;
    public LiveData<List<PlayerResult>> allPlayerResults;

    public PlayerResultRepository(Application application) {
        PlayerResultDatabase database = PlayerResultDatabase.getInstance(application);
        playerResultDao = database.playerResultDao();
        allPlayerResults = playerResultDao.getAllItems();
    }

    public void insert(final PlayerResult playerResult){
        new InsertPlayerResultAsync(playerResultDao).execute(playerResult);
    }

    public void update(PlayerResult playerResult){
        new UpdatePlayerResultAsync(playerResultDao).execute(playerResult);
    }

    public void delete(final PlayerResult playerResult){
        new DeletePlayerResultAsync(playerResultDao).execute(playerResult);
    }

    public LiveData<List<PlayerResult>> getAllPlayerResults() {
        return allPlayerResults;
    }

    public PlayerResult getById(int id) { return playerResultDao.getById(id); }

    private static class InsertPlayerResultAsync extends AsyncTask<PlayerResult, Void, Void> {
        private PlayerResultDao playerResultDao;

        private InsertPlayerResultAsync(PlayerResultDao playerResultDao) {
            this.playerResultDao = playerResultDao;
        }

        @Override
        protected Void doInBackground(PlayerResult... playerResults) {
            playerResultDao.insert(playerResults[0]);
            return null;
        }
    }

    private static class UpdatePlayerResultAsync extends AsyncTask<PlayerResult, Void, Void> {
        private PlayerResultDao playerResultDao;

        private UpdatePlayerResultAsync(PlayerResultDao playerResultDao) {
            this.playerResultDao = playerResultDao;
        }

        @Override
        protected Void doInBackground(PlayerResult... playerResults) {
            playerResultDao.update(playerResults[0]);
            return null;
        }
    }

    private static class DeletePlayerResultAsync extends AsyncTask<PlayerResult, Void, Void> {
        private PlayerResultDao playerResultDao;

        private DeletePlayerResultAsync(PlayerResultDao playerResultDao) {
            this.playerResultDao = playerResultDao;
        }

        @Override
        protected Void doInBackground(PlayerResult... playerResults) {
            playerResultDao.delete(playerResults[0]);
            return null;
        }
    }

}
