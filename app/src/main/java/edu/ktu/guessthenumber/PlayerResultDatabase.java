package edu.ktu.guessthenumber;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = PlayerResult.class, version = 2, exportSchema = false)
public abstract class PlayerResultDatabase extends RoomDatabase {

    private static  PlayerResultDatabase instance;

    public abstract PlayerResultDao playerResultDao();

    public static synchronized PlayerResultDatabase getInstance(Context context) {
        if(instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    PlayerResultDatabase.class, "player_result_db")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
      @Override
      public void onCreate(@NonNull SupportSQLiteDatabase db) {
          super.onCreate(db);
          new PopulateDbAsyncTask(instance).execute();
      }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private PlayerResultDao playerResultDao;

        private PopulateDbAsyncTask(PlayerResultDatabase db) {
            playerResultDao = db.playerResultDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            playerResultDao.insert(new PlayerResult("Player 1", 10,2, 3));
            playerResultDao.insert(new PlayerResult("Player 2", 15, 3, 0));
            return null;
        }
    }
}
