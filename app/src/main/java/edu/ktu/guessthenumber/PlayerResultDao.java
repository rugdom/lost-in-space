package edu.ktu.guessthenumber;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PlayerResultDao {

    @Insert
    void insert(PlayerResult playerResult);

    @Update
    void update(PlayerResult playerResult);

    @Delete
    void delete(PlayerResult delete);

    @Query("SELECT * FROM PlayerResult ORDER BY (winCount - loseCount) DESC")
    LiveData<List<PlayerResult>> getAllItems();

    @Query("SELECT * FROM PlayerResult WHERE id=:id ")
    PlayerResult getById(int id);
}
