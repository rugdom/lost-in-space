package edu.ktu.guessthenumber;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

public class PlayerListActivity extends AppCompatActivity {
    public static final int ADD_PLAYER_REQUEST = 1;
    private ResultViewModel playerViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_player_list);

        Button addPlayerButton = findViewById(R.id.btn_add_player);
        addPlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlayerListActivity.this, AddPlayerActivity.class);
                startActivityForResult(intent, ADD_PLAYER_REQUEST);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view_players);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        final PlayerViewAdapter adapter = new PlayerViewAdapter();
        recyclerView.setAdapter(adapter);

        playerViewModel = new ViewModelProvider(this).get(ResultViewModel.class);
        playerViewModel.getAllPlayerResults().observe(this, new Observer<List<PlayerResult>>() {
            @Override
            public void onChanged(List<PlayerResult> playerResults) {
                adapter.setPlayerResults(playerResults);
            }
        });

        adapter.setOnItemClickListener(new PlayerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(PlayerResult playerResult) {
                Intent data = getIntent();
                data.putExtra("chosenPlayer", (Serializable) playerResult);
                setResult(RESULT_OK, data);
                finish();
                //Toast.makeText(PlayerListActivity.this, playerResult.getId() + "", Toast.LENGTH_SHORT).show();
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                playerViewModel.delete(adapter.getPlayerResultAt(viewHolder.getAdapterPosition()));
                Toast.makeText(PlayerListActivity.this, R.string.player_deleted, Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_PLAYER_REQUEST && resultCode == RESULT_OK) {
            String playerName = data.getStringExtra(AddPlayerActivity.EXTRA_NAME);
            int playerAge = data.getIntExtra(AddPlayerActivity.EXTRA_AGE, 0);

            PlayerResult playerResult = new PlayerResult(playerName, playerAge, 0, 0);
            playerViewModel.insert(playerResult);
        }
    }
}
