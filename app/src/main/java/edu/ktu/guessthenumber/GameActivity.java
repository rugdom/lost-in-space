package edu.ktu.guessthenumber;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.List;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    private static final String PREFERENCES_FILE_NAME = "SettingsPref";
    private PlayerResultDao playerResultDao;

    // Easy
    private int minNumber = 0;
    private int maxNumber = 10;

    private int maxTurns = 7;
    private int randomNumber;
    private int currentTurn = 0;

    private int result;

    private TextView numberRangeText;
    private TextView resultText;
    private TextView turnsText;

    private EditText numberField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);

        numberRangeText = findViewById(R.id.range);
        resultText = findViewById(R.id.result);
        turnsText = findViewById(R.id.turns);
        numberField = findViewById(R.id.guess_field);

        SharedPreferences prefs = getSharedPreferences(PREFERENCES_FILE_NAME, MODE_PRIVATE);
        int difficulty = prefs.getInt("difficulty", 0);
        if(difficulty == 1){
            minNumber = 0;
            maxNumber = 30;
        }
        else if(difficulty == 2){
            minNumber = 0;
            maxNumber = 100;
        }
        else if (difficulty == 3) {
            minNumber = 0;
            maxNumber = 500;
        }

        updateTexts(0);

        Random random = new Random();
        randomNumber = random.nextInt(maxNumber - minNumber) + minNumber;
    }

    private void updateTexts(int guessedNumber) {
        numberRangeText.setText(String.format(getResources().getString(R.string.number_range_format), minNumber, maxNumber));
        if (result > 0) {
            resultText.setText(String.format(getResources().getString(R.string.result_format), guessedNumber, getResources().getString(R.string.result_high)));
        } else if (result < 0) {
            resultText.setText(String.format(getResources().getString(R.string.result_format), guessedNumber, getResources().getString(R.string.result_low)));
        }
        turnsText.setText(String.format(getResources().getString(R.string.turns_format), currentTurn, maxTurns));
    }

    public void guessClick(View view) {
        currentTurn++;
        int guessedNumber = Integer.parseInt(numberField.getText().toString());

        SharedPreferences prefs = getSharedPreferences(PREFERENCES_FILE_NAME, MODE_PRIVATE);
        int playerId = prefs.getInt("playerId", 0);

        result = 0;

        if (randomNumber > guessedNumber) {
            result = -1;
        } else if (randomNumber < guessedNumber) {
            result = 1;
        }

        if (currentTurn >= maxTurns && result != 0) {
            // Lose
            updateDB(playerId, false);

            Intent intent = new Intent(this, GameOverActivity.class);
            intent.putExtra("guessedNumber", guessedNumber);
            intent.putExtra("randomNumber", randomNumber);
            intent.putExtra("win", false);
            startActivity(intent);
            finish();
        } else if (result == 0) {
            // Win
            updateDB(playerId, true);

            Intent intent = new Intent(this, GameOverActivity.class);
            intent.putExtra("guessedNumber", guessedNumber);
            intent.putExtra("randomNumber", randomNumber);
            intent.putExtra("currentTurn", currentTurn);
            intent.putExtra("win", true);
            startActivity(intent);
            finish();
        }

        updateTexts(guessedNumber);
    }

    private void updateDB(final int playerId, final boolean won) {
        playerResultDao = Room.databaseBuilder(this, PlayerResultDatabase.class, "player_result_db")
                .allowMainThreadQueries()
                .build()
                .playerResultDao();

        PlayerResult playerResult = playerResultDao.getById(playerId);

        //Toast.makeText(this, playerId + " " + playerResult.getId(), Toast.LENGTH_SHORT).show();
        if(playerResult != null) {
            if (won) {
                playerResult.setWinCount(playerResult.getWinCount() + 1);
                playerResultDao.update(playerResult);
                //Toast.makeText(this, playerResult.getWinCount() + "", Toast.LENGTH_SHORT).show();
            } else {
                playerResult.setLoseCount(playerResult.getLoseCount() + 1);
                playerResultDao.update(playerResult);
                //Toast.makeText(this, playerResult.getLoseCount() + "", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
