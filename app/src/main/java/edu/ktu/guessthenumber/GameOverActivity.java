package edu.ktu.guessthenumber;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class GameOverActivity extends AppCompatActivity {
    private static final String PREFERENCES_FILE_NAME = "SettingsPref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game_over);

        TextView result = findViewById(R.id.result_text);
        TextView result_information = findViewById(R.id.game_over_text);
        ImageView image = findViewById(R.id.game_over);

        Intent intent = getIntent();
        boolean win = intent.getBooleanExtra("win", false);
        int randomNumber = intent.getIntExtra("randomNumber", 0);
        int currentTurn = intent.getIntExtra("currentTurn", 0);

        SharedPreferences prefs = getSharedPreferences(PREFERENCES_FILE_NAME, MODE_PRIVATE);
        boolean sound = prefs.getBoolean("sound", true);

        MediaPlayer winSound = MediaPlayer.create(getApplicationContext(), R.raw.win);
        MediaPlayer loseSound = MediaPlayer.create(getApplicationContext(), R.raw.lose);

        if(win)
        {
            if(sound) {
                winSound.start();
            }

            result.setText(getResources().getString(R.string.player_won));
            result_information.setText(getResources().getString(R.string.player_won_information, randomNumber, currentTurn));
        }
        else
        {
            if(sound) {
                loseSound.start();
            }

            result.setText(getResources().getString(R.string.player_lost));
            result_information.setText(getResources().getString(R.string.player_lost_information, randomNumber));
            image.setImageResource(R.drawable.player_lost);
        }
    }
}
