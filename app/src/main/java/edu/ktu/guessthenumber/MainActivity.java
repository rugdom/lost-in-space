package edu.ktu.guessthenumber;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

    }

    public void startClick(View view) {
        if(view == findViewById(R.id.start_game_btn)) {
            Intent intent = new Intent(this, GameActivity.class);
            startActivity(intent);
        }
        else if(view == findViewById(R.id.results_btn)) {
            Intent intent = new Intent(this, ResultActivity.class);
            startActivity(intent);
        }
        else if(view == findViewById(R.id.settings_btn)) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        else if(view == findViewById(R.id.about_btn)) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }
    }

}
