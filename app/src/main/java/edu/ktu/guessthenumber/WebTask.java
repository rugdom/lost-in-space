package edu.ktu.guessthenumber;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WebTask extends AsyncTask<String, Void, String> {

    public interface WebRequestFiniched
    {
        void webRequestResult(String result);
    }

    WebRequestFiniched webRequestFiniched;

    public void setWebRequestFiniched(WebRequestFiniched inObj)
    {
        webRequestFiniched = inObj;
    }

    // Atlieka visus klasės veiksmus; visa logika
    @Override
    protected String doInBackground(String... url) {
        String result = "";
        if(url.length > 0) {
            try {
                URL urlObject = new URL(url[0]);
                HttpURLConnection httpURLConnection =
                        (HttpURLConnection)urlObject.openConnection();
                InputStream in = httpURLConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line = null;
                while((line = reader.readLine()) != null){
                    result += line;
                }
                httpURLConnection.disconnect();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return result;
    }

    // Grąžinamas doInBackground rezultatas
    @Override
    protected void onPostExecute(String s) {
        if(webRequestFiniched != null){
            webRequestFiniched.webRequestResult((s));
        }
    }
}
