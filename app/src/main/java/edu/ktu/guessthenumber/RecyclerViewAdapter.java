package edu.ktu.guessthenumber;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<PlayerResult> playerResults = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_result_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlayerResult currentPlayerResult = playerResults.get(position);
        holder.name.setText(currentPlayerResult.getName());
        holder.winCount.setText(String.valueOf(
                currentPlayerResult.getWinCount()));
        holder.loseCount.setText(String.valueOf(
                currentPlayerResult.getLoseCount()));
    }

    @Override
    public int getItemCount() {
        return playerResults.size();
    }

    public void setPlayerResults(List<PlayerResult> playerResults) {
        this.playerResults = playerResults;
        notifyDataSetChanged();
    }

    public PlayerResult getPlayerResultAt(int position) {
        return  playerResults.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView winCount;
        public TextView loseCount;
        public ViewHolder(View itemView)
        {
            super(itemView);
            this.name = itemView.findViewById(R.id.result_player_name);
            this.winCount = itemView.findViewById(R.id.result_win_count);
            this.loseCount = itemView.findViewById(R.id.result_lose_count);
        }
    }

}
