package edu.ktu.guessthenumber;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ResultViewModel extends AndroidViewModel {

    private PlayerResultRepository repository;
    private LiveData<List<PlayerResult>> allPlayerResults;

    public ResultViewModel(@NonNull Application application) {
        super(application);
        repository = new PlayerResultRepository(application);
        allPlayerResults = repository.getAllPlayerResults();
    }

    public void insert(PlayerResult playerResult) {
        repository.insert(playerResult);
    }

    public void update(PlayerResult playerResult) {
        repository.update(playerResult);
    }

    public void delete(PlayerResult playerResult) {
        repository.delete(playerResult);
    }

    public LiveData<List<PlayerResult>> getAllPlayerResults() {
        return allPlayerResults;
    }

    public PlayerResult getById(int id) { return repository.getById(id); }
}
