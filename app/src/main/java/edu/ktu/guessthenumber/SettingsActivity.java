package edu.ktu.guessthenumber;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {
    public static final int CHOOSE_PLAYER_REQUEST = 2;
    private static final String PREFERENCES_FILE_NAME = "SettingsPref";

    private AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorWhite));
            ((TextView) parent.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_settings);

        SharedPreferences prefs = getSharedPreferences(PREFERENCES_FILE_NAME, MODE_PRIVATE);

        String playerName = prefs.getString("playerName", getResources().getString(R.string.default_name));
        int playerId = prefs.getInt("playerId", 0);
        int playerAge = prefs.getInt("playerAge", 1);
        int difficulty = prefs.getInt("difficulty", 0);
        boolean sound = prefs.getBoolean("sound", true);

        TextView nameField = findViewById(R.id.player_name_field);
        TextView ageField = findViewById(R.id.player_age_field);
        Spinner spinner = findViewById(R.id.difficulty_spinner);
        Switch soundSwitch = findViewById(R.id.sound_switch);

        spinner.setOnItemSelectedListener(listener);

        nameField.setText(playerName);
        ageField.setText(Integer.toString(playerAge));
        spinner.setSelection(difficulty);
        soundSwitch.setChecked(sound);
    }

    public void saveSettings(View v)
    {
        TextView playerNameField = findViewById(R.id.player_name_field);
        TextView playerAgeField = findViewById(R.id.player_age_field);
        Spinner spinner = findViewById(R.id.difficulty_spinner);
        Switch soundSwitch = findViewById(R.id.sound_switch);

        String playerName = playerNameField.getText().toString();
        int playerAge = Integer.parseInt(playerAgeField.getText().toString());
        int difficulty = spinner.getSelectedItemPosition();
        boolean sound = soundSwitch.isChecked();

        if(sound){
            unmute();
        } else {
            mute();
        }

        // Editor leidžia keisti reikšmes
        SharedPreferences.Editor prefs = getSharedPreferences(PREFERENCES_FILE_NAME, MODE_PRIVATE).edit();

        prefs.putString("playerName", playerName);
        prefs.putInt("playerAge", playerAge);
        prefs.putInt("difficulty", difficulty);
        prefs.putBoolean("sound", sound);

        // galima ir commit, bet geriau apply
        prefs.apply();

        finish();
    }

    public void startClick(View view) {
        if (view == findViewById(R.id.btn_choose_player)) {
            Intent intent = new Intent(SettingsActivity.this, PlayerListActivity.class);
            startActivityForResult(intent, CHOOSE_PLAYER_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Toast.makeText(this, requestCode + " " + resultCode, Toast.LENGTH_SHORT).show();
        if(requestCode == CHOOSE_PLAYER_REQUEST && resultCode == RESULT_OK) {
            Serializable playerResult = data.getSerializableExtra("chosenPlayer");
            PlayerResult res = (PlayerResult) playerResult;

            TextView playerNameField = findViewById(R.id.player_name_field);
            TextView playerAgeField = findViewById(R.id.player_age_field);
            playerNameField.setText(res.getName());
            playerAgeField.setText(Integer.toString(res.getAge()));

            SharedPreferences.Editor prefs = getSharedPreferences(PREFERENCES_FILE_NAME, MODE_PRIVATE).edit();
            prefs.putInt("playerId", res.getId());
            prefs.apply();
        }
    }

    private void mute() {
        //mute audio
        AudioManager amanager = (AudioManager) getSystemService(getApplicationContext().AUDIO_SERVICE);
        amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
    }

    public void unmute() {
        //unmute audio
        AudioManager amanager = (AudioManager) getSystemService(getApplicationContext().AUDIO_SERVICE);
        amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
    }
}
